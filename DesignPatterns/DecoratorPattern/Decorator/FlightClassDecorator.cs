﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.DecoratorPattern
{
    public class FlightClassDecorator : IFlightClass
    {
        protected IFlightClass flightClass;

        public FlightClassDecorator(IFlightClass FlightClass)
        {
            this.flightClass = FlightClass;
        }

        public int MaxWeightOfLuggage { get; set; }
        public bool FastTracking { get; set; }
        public List<string> Meals { get; set; }

        public void DisplayClassOfFlight()
        {
            flightClass.DisplayClassOfFlight();
            Console.WriteLine("Info from concrete decorator");
        }

        public void SetMeals()
        {
            flightClass.SetMeals();
        }
    }
}
