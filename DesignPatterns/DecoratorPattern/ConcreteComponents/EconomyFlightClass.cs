﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.DecoratorPattern
{
    public class EconomyFlightClass : IFlightClass
    {
        public int MaxWeightOfLuggage { get; set; }
        public bool FastTracking { get; set; }
        public List<string> Meals { get; set; }

        public void DisplayClassOfFlight()
        {
            Console.WriteLine("Economy Class");
        }

        public void SetMeals()
        {
            this.Meals = new List<string> {
                "Lunch"
            };
        }
    }
}
