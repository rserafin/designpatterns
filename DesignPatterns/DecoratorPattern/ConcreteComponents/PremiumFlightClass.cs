﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.DecoratorPattern
{
    public class PremiumFlightClass : IFlightClass
    {
        public int MaxWeightOfLuggage { get; set; }
        public bool FastTracking { get; set; }
        public List<string> Meals { get; set; }

        public void DisplayClassOfFlight()
        {
            Console.WriteLine("Premium Class");
        }

        public void SetMeals()
        {
            this.Meals = new List<string> {
                "Lunch",
                "Sandwich"
            };
        }
    }
}
