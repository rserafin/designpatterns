﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.DecoratorPattern
{
    public class BusinessFlightClass : IFlightClass
    {
        public int MaxWeightOfLuggage { get; set; }
        public bool FastTracking { get; set; }
        public List<string> Meals { get; set; }

        public void DisplayClassOfFlight()
        {
            Console.WriteLine("Business Class");
        }

        public void SetMeals()
        {
            this.Meals = new List<string> {
                "Lunch",
                "Dessert",
                "Sandwich",
                "Snacks"
            };
        }
    }
}
