﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.DecoratorPattern
{
    public class RentalCarDecorator : FlightClassDecorator
    {
        Dictionary<string, decimal> priceList = new Dictionary<string, decimal>();

        public RentalCarDecorator(IFlightClass flightClass) : base(flightClass)
        {
        }

        public void DisplayPricelist()
        {
            SetPricelist();

            foreach (var item in priceList)
            {
                Console.WriteLine("Price: {0}, Model: {1}", item.Value, item.Key);
            }
        }

        private void SetPricelist()
        {
            this.priceList.Add("Audi A8", 50);
            this.priceList.Add("Lexus IS", 0);
        }
    }
}
