﻿using System.Collections.Generic;

namespace DesignPatterns.DecoratorPattern
{
    public interface IFlightClass
    {
        int MaxWeightOfLuggage { get; set; }
        bool FastTracking { get; set; }
        List<string> Meals { get; set; }

        void DisplayClassOfFlight();
        void SetMeals();
    }
}
