﻿using DesignPatterns.BuilderPattern.Builder;

namespace DesignPatterns.BuilderPattern.FluentBuilder
{
    public interface IStudentLiabilitiesFluentBuilder
    {
        IStudentLiabilitiesFluentBuilder BuildCourseFees();
        IStudentLiabilitiesFluentBuilder BuildInsuranceFees(bool additionalInsurance);
        IStudentLiabilitiesFluentBuilder BuildAcademicFees();
        StudentLiabilities GetStudentLiabilities();
    }
}
