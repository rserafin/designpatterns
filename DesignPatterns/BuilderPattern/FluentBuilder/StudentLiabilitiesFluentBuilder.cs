﻿using DesignPatterns.BuilderPattern.Builder;

namespace DesignPatterns.BuilderPattern.FluentBuilder
{
    public class StudentLiabilitiesFluentBuilder : IStudentLiabilitiesFluentBuilder
    {
        private StudentLiabilities _studentLiabilities;

        public StudentLiabilitiesFluentBuilder()
        {
            _studentLiabilities = new StudentLiabilities();
        }

        public IStudentLiabilitiesFluentBuilder BuildAcademicFees()
        {
            //here should be more sophisticated logic
            _studentLiabilities.AcademicFees = 275m;
            return this;
        }

        public IStudentLiabilitiesFluentBuilder BuildCourseFees()
        {
            //here should be more sophisticated logic
            _studentLiabilities.CourseFees = 500.0m + 350.50m;
            return this;
        }

        public IStudentLiabilitiesFluentBuilder BuildInsuranceFees(bool additionalInsurance)
        {
            //here should be more sophisticated logic
            _studentLiabilities.InsuranceFees = 999.9m;
            if (additionalInsurance)
            {
                _studentLiabilities.InsuranceFees += 300m;
            }
            return this;
        }

        public StudentLiabilities GetStudentLiabilities()
        {
            return _studentLiabilities;
        }
    }
}
