﻿namespace DesignPatterns.BuilderPattern.Builder
{
    public class StudentLiabilitiesBuilder : IStudentLiabilitiesBuilder
    {
        private StudentLiabilities _studentLiabilities;

        public StudentLiabilitiesBuilder(StudentLiabilities studentLiabilities)
        {
            _studentLiabilities = studentLiabilities;
        }

        public void BuildAcademicFees()
        {
            //here should be more sophisticated logic
            _studentLiabilities.AcademicFees = 275m;
        }

        public void BuildCourseFees()
        {
            //here should be more sophisticated logic
            _studentLiabilities.CourseFees = 500.0m + 350.50m;
        }

        public void BuildInsuranceFees()
        {
            //here should be more sophisticated logic
            _studentLiabilities.InsuranceFees = 999.9m;
        }

        public StudentLiabilities GetStudentLiabilities()
        {
            return _studentLiabilities;
        }
    }
}
