﻿namespace DesignPatterns.BuilderPattern.Builder
{
    public interface IStudentLiabilitiesBuilder
    {
        void BuildCourseFees();
        void BuildInsuranceFees();
        void BuildAcademicFees();
        StudentLiabilities GetStudentLiabilities();
    }
}
