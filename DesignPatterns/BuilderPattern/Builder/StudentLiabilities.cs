﻿namespace DesignPatterns.BuilderPattern.Builder
{
    public class StudentLiabilities
    {
        public decimal CourseFees { get; set; }
        public decimal InsuranceFees { get; set; }
        public decimal AcademicFees { get; set; }
    }
}
