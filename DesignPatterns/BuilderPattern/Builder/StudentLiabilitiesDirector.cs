﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.BuilderPattern.Builder
{
    public class StudentLiabilitiesDirector
    {
        private readonly IStudentLiabilitiesBuilder _studentLiabilitiesBuilder;

        public StudentLiabilitiesDirector(IStudentLiabilitiesBuilder builder)
        {
            _studentLiabilitiesBuilder = builder;
        }

        public StudentLiabilities Build()
        {
            _studentLiabilitiesBuilder.BuildCourseFees();
            _studentLiabilitiesBuilder.BuildAcademicFees();
            _studentLiabilitiesBuilder.BuildInsuranceFees();
            return _studentLiabilitiesBuilder.GetStudentLiabilities();
        }
    }
}
