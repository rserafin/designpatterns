﻿using DesignPatterns.StrategyPattern.ConcreteStrategies;

namespace DesignPatterns.StrategyPattern
{
    public class DormitoryFactory
    {
        public IDormitoryCalculationStrategy GetDormitoryCalculationStrategy(DormitoryCountry dormitoryCountry)
        {
            switch (dormitoryCountry)
            {
                case DormitoryCountry.UK: return new UKDormitoryCalculation();
                case DormitoryCountry.USA: return new USDormitoryCalculation();
                case DormitoryCountry.Canada: return new CanadaDormitoryCalculation();
                default: return null;
            }
        }
    }
}
