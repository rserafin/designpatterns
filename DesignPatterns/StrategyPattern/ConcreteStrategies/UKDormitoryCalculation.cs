﻿namespace DesignPatterns.StrategyPattern.ConcreteStrategies
{
    class UKDormitoryCalculation : IDormitoryCalculationStrategy
    {
        public decimal CalculatePrice()
        {
            return 100m * 1.2m;
        }
    }
}
