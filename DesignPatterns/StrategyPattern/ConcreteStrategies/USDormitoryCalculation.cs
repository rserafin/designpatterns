﻿namespace DesignPatterns.StrategyPattern.ConcreteStrategies
{
    class USDormitoryCalculation : IDormitoryCalculationStrategy
    {
        public decimal CalculatePrice()
        {
            return 100m * 1.3m;
        }
    }
}
