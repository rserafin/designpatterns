﻿namespace DesignPatterns.StrategyPattern.ConcreteStrategies
{
    class CanadaDormitoryCalculation : IDormitoryCalculationStrategy
    {
        public decimal CalculatePrice()
        {
            return 100m * 1.25m;
        }
    }
}
