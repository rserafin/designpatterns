﻿namespace DesignPatterns.StrategyPattern
{
    public interface IDormitoryCalculationStrategy
    {
        decimal CalculatePrice();
    }
}
