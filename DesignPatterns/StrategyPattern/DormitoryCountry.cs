﻿namespace DesignPatterns.StrategyPattern
{
    public enum DormitoryCountry
    {
        UK,
        USA,
        Canada
    }
}
