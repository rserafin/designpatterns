﻿using DesignPatterns.StrategyPattern.ConcreteStrategies;

namespace DesignPatterns.StrategyPattern
{
    public class DormitoryCalculator_WithoutFactory
    {
        //stratagies for calculating interest without using Factory pattern
        private IDormitoryCalculationStrategy canadaDormitoryStrategy = new CanadaDormitoryCalculation();
        private IDormitoryCalculationStrategy ukDormitoryStrategy = new UKDormitoryCalculation();
        private IDormitoryCalculationStrategy usDormitoryStrategy = new USDormitoryCalculation();

        public decimal CalculatePrice(DormitoryCountry country)
        {
            switch (country)
            {
                case DormitoryCountry.UK: return ukDormitoryStrategy.CalculatePrice();                    
                case DormitoryCountry.USA: return usDormitoryStrategy.CalculatePrice();                    
                case DormitoryCountry.Canada: return canadaDormitoryStrategy.CalculatePrice();                    
                default: return 0m;                    
            }
        }
    }
}
