﻿namespace DesignPatterns.StrategyPattern
{
    public class DormitoryCalculator_WithFactory
    {
        private readonly DormitoryFactory _dormitoryFactory;
        public DormitoryCalculator_WithFactory()
        {
            _dormitoryFactory = new DormitoryFactory();
        }

        public decimal CalculatePrice(DormitoryCountry country)
        {
            var strategy = _dormitoryFactory.GetDormitoryCalculationStrategy(country);
            return strategy.CalculatePrice();
        }
    }
}
