﻿using EF_CodeFirst;
using EF_CodeFirst.Entities;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DesignPatterns.RepositoryPattern
{
    public class Repository<T, C> : IRepository<T> where T : Entity where C : DbContext,new()
    {       
        private C context;

        public Repository(C context)
        {
            this.context = context;
        }

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = context.Set<T>();
            return query;
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = context.Set<T>().Where(predicate);
            return query;
        }

        public virtual T FindByID(int id)
        {
            T entity = context.Set<T>().FirstOrDefault(x => x.Id == id);
            return entity;
        }

        public virtual void Add(T entity)
        {
            context.Set<T>().Add(entity);
        }

        public virtual void Remove(T entity)
        {
            context.Set<T>().Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }
    }
}
