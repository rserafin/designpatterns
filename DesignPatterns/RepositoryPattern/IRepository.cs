﻿using EF_CodeFirst.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace DesignPatterns.RepositoryPattern
{
    public interface IRepository<T> where T : Entity
    {
        IQueryable<T> GetAll();
        T FindByID(int id);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Remove(T entity);
        void Edit(T entity);
    }
}
