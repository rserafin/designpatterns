﻿using System;

namespace DesignPatterns.ChainOfResponsibility
{
    public class ReservationStudentQualification : StudentQualification
    {
        public override void QualifyStudent(StudentApplication studentApplication)
        {
            if (studentApplication.IsReserved)
            {
                Console.WriteLine("Student is assigned on student list because he was on reservation list.");
            }
            else {
                Next.QualifyStudent(studentApplication);
            }
        }
    }
}
