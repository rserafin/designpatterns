﻿using System;

namespace DesignPatterns.ChainOfResponsibility
{
    public class RegistrationStudentQualification : StudentQualification
    {
        public override void QualifyStudent(StudentApplication studentApplication)
        {
            if (!studentApplication.IsReserved && studentApplication.AmountOfPlaces > 0)
            {
                Console.WriteLine("Student can be assigned on student list during normal registration process.");
            }
            else {
                Next.QualifyStudent(studentApplication);
            }
        }
    }
}
