﻿namespace DesignPatterns.ChainOfResponsibility
{
    public abstract class StudentQualification
    {        
        protected StudentQualification Next;

        public void SetNext(StudentQualification next)
        {
            this.Next = next;
        }

        public abstract void QualifyStudent(StudentApplication studentApplication);
    }
}
