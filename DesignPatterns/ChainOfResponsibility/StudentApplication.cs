﻿namespace DesignPatterns.ChainOfResponsibility
{
    public class StudentApplication
    {
        public StudentApplication(string studentName, bool isReserved, int amountOfPlaces)
        {
            this.StudentName = studentName;
            this.IsReserved = isReserved;
            this.AmountOfPlaces = amountOfPlaces;
        }

        public string StudentName { get; set; }
        public bool IsReserved { get; set; }
        public int AmountOfPlaces { get; set; } //it should be get from database
    }
}
