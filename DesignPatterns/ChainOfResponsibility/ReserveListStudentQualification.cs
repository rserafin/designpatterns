﻿using System;

namespace DesignPatterns.ChainOfResponsibility
{
    public class ReserveListStudentQualification : StudentQualification
    {
        public override void QualifyStudent(StudentApplication studentApplication)
        {
            if (!studentApplication.IsReserved && studentApplication.AmountOfPlaces < 1)
            {
                Console.WriteLine("Student didn't make reservation and there are no seats available.");
            }
        }
    }
}
