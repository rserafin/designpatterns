﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.FacadePattern
{
    public class Invoice
    {
        public Guid InvoiceGID { get; set; }
        public DateTime DateOfInvoice { get; set; }
        public int TaxIdentificationNumberOfSeller { get; set; }
        public int TaxIdentificationNumberOfBuyer { get; set; }
        public decimal GeneralAmount { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public int OrderId { get; set; }
    }
}
