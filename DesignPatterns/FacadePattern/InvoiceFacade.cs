﻿namespace DesignPatterns.FacadePattern
{
    public class InvoiceFacade : IInvoiceFacade
    {
        IInvoiceManager _invoiceManager;
        IOrderManager _orderManager;

        public InvoiceFacade()
        {
            _invoiceManager = new InvoiceManager();
            _orderManager = new OrderManager();
        }

        public Invoice GenerateInvoice(int orderId)
        {
            var invoice = new Invoice();
            invoice.InvoiceGID = _invoiceManager.GenerateInvoiceNumber();
            invoice.OrderId = orderId;
            invoice.TaxIdentificationNumberOfBuyer = 100;
            invoice.TaxIdentificationNumberOfSeller = 101;
            invoice.OrderItems = _orderManager.GetOrderItems(orderId);
            invoice.GeneralAmount = _orderManager.SumAmountOfOrder(invoice.OrderItems);
            invoice = _invoiceManager.TakeIntoAccountAdvanceInvoice(invoice);

            return invoice;
        }
    }
}
