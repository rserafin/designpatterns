﻿using System.Collections.Generic;

namespace DesignPatterns.FacadePattern
{
    interface IOrderManager
    {
        List<OrderItem> GetOrderItems(int orderId);
        decimal SumAmountOfOrder(List<OrderItem> orderItems);        
    }
}
