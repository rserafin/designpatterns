﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.FacadePattern
{
    interface IInvoiceManager
    {
        Guid GenerateInvoiceNumber();
        Invoice TakeIntoAccountAdvanceInvoice(Invoice invoice);
    }
}
