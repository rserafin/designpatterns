﻿using System;

namespace DesignPatterns.FacadePattern
{
    public class OrderItem
    {
        public int OrderID { get; set; }
        public Guid ItemIdentifier { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TaxRate { get; set; }
    }
}
