﻿namespace DesignPatterns.FacadePattern
{
    public class Address
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public string ApartmentNumber { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
    }
}
