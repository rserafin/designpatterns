﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.FacadePattern
{
    public class InvoiceManager : IInvoiceManager
    {
        public Guid GenerateInvoiceNumber()
        {
            return Guid.NewGuid();
        }

        public Invoice TakeIntoAccountAdvanceInvoice(Invoice invoice)
        {
            invoice.GeneralAmount -= this.GetAdvanceInvoice(invoice.OrderId).Sum(advanceInvoice => advanceInvoice.AdvanceAmount);
            return invoice;
        }

        private List<AdvanceInvoice> GetAdvanceInvoice(int orderId)
        {
            //here e.g. should be access to database to load invoice
            List< AdvanceInvoice> advanceInvoices = new List<AdvanceInvoice>
                {
                new AdvanceInvoice() { AdvanceAmount = 150 }
                };

            return advanceInvoices;
        }
    }
}
