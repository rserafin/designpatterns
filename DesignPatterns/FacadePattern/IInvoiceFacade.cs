﻿namespace DesignPatterns.FacadePattern
{
    interface IInvoiceFacade
    {
        Invoice GenerateInvoice(int orderId);
    }
}
