﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.FacadePattern
{
    public class OrderManager : IOrderManager
    {
        public List<OrderItem> GetOrderItems(int orderId)
        {
            //hardcoded values in order to simplify an implementation
            var orderItems = new List<OrderItem>
            {
                new OrderItem(){  Amount = 10, ItemIdentifier = Guid.NewGuid(), Name = "Product 1", OrderID = orderId, TaxRate = 0.05M, UnitPrice = 11.99M},
                new OrderItem(){  Amount = 3, ItemIdentifier = Guid.NewGuid(), Name = "Product 2", OrderID = orderId, TaxRate = 0.21M, UnitPrice = 89M }
            };

            return orderItems;
        }
        public decimal SumAmountOfOrder(List<OrderItem> orderItems)
        {
            return orderItems.Sum(orderItem => orderItem.Amount * orderItem.UnitPrice);
        }
    }
}
