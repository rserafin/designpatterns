﻿namespace DesignPatterns.FacadePattern
{
    public class AdvanceInvoice : Invoice
    {
        public decimal AdvanceAmount { get; set; }
    }
}
