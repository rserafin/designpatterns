﻿namespace DesignPatterns.Singleton
{
    // Bad code! Do not use!
    // Two different threads could both have evaluated the test if (instance==null) and found it to be true, then both create instances, which violates the singleton pattern. Note that in fact the instance may already have been created before the expression is evaluated, but the memory model doesn't guarantee that the new value of instance will be seen by other threads unless suitable memory barriers have been passed.
    public sealed class SingletonNotThreadSafe
    {
        private static SingletonNotThreadSafe instance = null;

        private SingletonNotThreadSafe()
        {
        }

        public static SingletonNotThreadSafe Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SingletonNotThreadSafe();
                }
                return instance;
            }
        }
    }
}
