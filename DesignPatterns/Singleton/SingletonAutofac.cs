﻿namespace DesignPatterns.Singleton
{
    public class SingletonAutofac
    {
        public string NameTest { get; private set; }
        public SingletonAutofac()
        {
            this.NameTest = "Rafał Serafin";
        }
    }
}
