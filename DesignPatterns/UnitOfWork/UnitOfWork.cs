﻿using DesignPatterns.RepositoryPattern;
using EF_CodeFirst;
using EF_CodeFirst.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.UnitOfWork
{
    public class UnitOfWork : IDisposable
    {
        private CodeFirstContext context = null;

        public UnitOfWork()
        {
            context = new CodeFirstContext();
        }

        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public IRepository<T> Repository<T>() where T : Entity
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IRepository<T>;
            }
            IRepository<T> repo = new Repository<T,CodeFirstContext>(context);
            repositories.Add(typeof(T), repo);
            return repo;
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
