﻿using DesignPatterns.FactoryPattern.AbstractFactory.ConcreteClasses;

namespace DesignPatterns.FactoryPattern.AbstractFactory.Factories
{
    public class DormitoryFactory : IResidenceFactory
    {
        public IRoom CreateRoom()
        {
            return new DormitoryRoom();
        }
        public IBedroom CreateBedroom()
        {
            return new DormitoryBedroom();
        }
    }
}
