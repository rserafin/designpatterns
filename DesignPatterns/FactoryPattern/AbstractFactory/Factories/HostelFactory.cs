﻿using DesignPatterns.FactoryPattern.AbstractFactory.ConcreteClasses;

namespace DesignPatterns.FactoryPattern.AbstractFactory.Factories
{
    public class HostelFactory : IResidenceFactory
    {
        public IRoom CreateRoom()
        {
            return new HostelRoom();
        }
        public IBedroom CreateBedroom()
        {
            return new HostelBedroom();
        }
    }
}
