﻿using DesignPatterns.FactoryPattern.AbstractFactory.Factories;

namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public class AbstractFactory
    {
        public static IResidenceFactory GetFactory(bool isHostel)
        {
            if (isHostel)
            {
                return new HostelFactory();
            }
            else {
                return new DormitoryFactory();
            }
        }
    }
}
