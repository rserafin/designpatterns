﻿namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public interface IResidenceFactory
    {
        IRoom CreateRoom();
        IBedroom CreateBedroom();
    }
}
