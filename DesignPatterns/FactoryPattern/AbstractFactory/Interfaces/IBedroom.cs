﻿using System.Collections.Generic;

namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public interface IBedroom
    {
        bool PrivateBedroom { get; }
        List<string> GetEquipment();
    }
}
