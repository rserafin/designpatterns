﻿namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public interface IRoom
    {
        int NumberOfBeds { get; }
        int GetPrice();
        int GetPricePerBed();
    }
}
