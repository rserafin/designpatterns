﻿namespace DesignPatterns.FactoryPattern.AbstractFactory.ConcreteClasses
{
    public class HostelRoom : IRoom
    {
        public HostelRoom()
        {
            this.NumberOfBeds = 4;
        }
        public int NumberOfBeds { get; private set; }

        public int GetPrice()
        {
            return 200;
        }

        public int GetPricePerBed()
        {
            return GetPrice() / this.NumberOfBeds;
        }
    }
}
