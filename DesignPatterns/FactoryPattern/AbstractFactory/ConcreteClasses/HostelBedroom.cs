﻿using System.Collections.Generic;

namespace DesignPatterns.FactoryPattern.AbstractFactory.ConcreteClasses
{
    public class HostelBedroom : IBedroom
    {
        public HostelBedroom()
        {
            this.PrivateBedroom = false;
        }
        public bool PrivateBedroom { get; }

        public List<string> GetEquipment()
        {
            List<string> equipment = new List<string>();
            equipment.Add("Dryer");
            return equipment;
        }
    }
}
