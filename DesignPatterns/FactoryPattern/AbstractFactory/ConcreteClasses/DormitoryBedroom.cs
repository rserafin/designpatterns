﻿using System.Collections.Generic;

namespace DesignPatterns.FactoryPattern.AbstractFactory.ConcreteClasses
{
    public class DormitoryBedroom : IBedroom
    {
        public DormitoryBedroom()
        {
            this.PrivateBedroom = false;
        }
        public bool PrivateBedroom { get; }

        public List<string> GetEquipment()
        {
            return new List<string>();
        }
    }
}
