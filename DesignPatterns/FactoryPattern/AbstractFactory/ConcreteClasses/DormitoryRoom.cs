﻿namespace DesignPatterns.FactoryPattern.AbstractFactory.ConcreteClasses
{
    public class DormitoryRoom : IRoom
    {
        public DormitoryRoom()
        {
            this.NumberOfBeds = 8;
        }
        public int NumberOfBeds { get; private set; }

        public int GetPrice()
        {
            return 160;
        }

        public int GetPricePerBed()
        {
            return GetPrice() / this.NumberOfBeds;
        }
    }
}
