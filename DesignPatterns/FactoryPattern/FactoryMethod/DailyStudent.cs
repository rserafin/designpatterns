﻿using System;

namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class DailyStudent : IStudent
    {
        public int AcademicYear { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public void DisplayFullName()
        {
            Console.WriteLine("DailyStudent: {0} {1}", this.FirstName, this.Surname);
        }

        public int GetTimeOfClasses()
        {
            return 40;
        }
    }
}
