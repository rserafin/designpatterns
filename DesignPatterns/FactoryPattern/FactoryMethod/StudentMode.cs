﻿namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public enum StudentMode
    {
        Daily = 1,
        Evening,
        PartTime
    }
}
