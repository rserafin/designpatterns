﻿using System;

namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class StudentFactory 
    {
        public IStudent GetStudent(StudentMode mode)
        {
            switch (mode)
            {
                case StudentMode.Daily:
                    return new DailyStudent();                    
                case StudentMode.Evening:
                    return new EveningStudent();
                case StudentMode.PartTime:
                    return new PartTimeStudent();
                default:
                    throw new ArgumentOutOfRangeException("Value of parameter is beyond StudentMode");
            }
        }
    }
}
