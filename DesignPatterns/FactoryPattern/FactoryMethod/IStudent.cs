﻿namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public interface IStudent
    {
        int AcademicYear { get; set; }
        string FirstName { get; set; }
        string Surname { get; set; }

        void DisplayFullName();
        int GetTimeOfClasses();
    }
}
