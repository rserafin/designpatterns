﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class PartTimeStudent : IStudent
    {
        public int AcademicYear { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public void DisplayFullName()
        {
            Console.WriteLine("DailyStudent: {0} {1}", this.FirstName, this.Surname);
        }

        public int GetTimeOfClasses()
        {
            return 25;
        }
    }
}
