﻿namespace DesignPatterns.CompositePattern
{
    //TeamLeader is composite it means TeamLeader can have subordinatess
    public class TeamLeader : EmployeeComponent
    {
        public TeamLeader(string name) : base(name)
        {
        }

        public override void AddSubordinate(EmployeeComponent employeeComponent)
        {
            Subordinates.Add(employeeComponent);
        }

        public override void RemoveSubordinate(EmployeeComponent employeeComponent)
        {
            Subordinates.Remove(employeeComponent);
        }
    }
}
