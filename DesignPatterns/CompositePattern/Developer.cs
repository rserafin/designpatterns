﻿using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.CompositePattern
{
    //Developer is leaf beacuse developer can't have any subordinates
    public class Developer : EmployeeComponent
    {
        public Developer(string name) : base(name)
        {
        }
    }
}
