﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.CompositePattern
{
    public abstract class EmployeeComponent
    {
        public EmployeeComponent(string name)
        {
            Subordinates = new List<EmployeeComponent>();
            Name = name;
        }

        public List<EmployeeComponent> Subordinates { get; set; }
        public string Name { get; set; }

        public virtual int CountSubordinates()
        {
            return Subordinates.Count();
        }

        public virtual void AddSubordinate(EmployeeComponent employeeComponent)
        {
            throw new NotImplementedException();
        }

        public virtual void RemoveSubordinate(EmployeeComponent employeeComponent)
        {
            throw new NotImplementedException();
        }
    }
}
