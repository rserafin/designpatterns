﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.IteratorPattern;
using EF_CodeFirst;
using DesignPatterns.RepositoryPattern;
using EF_CodeFirst.Entities;
using DesignPatterns.UnitOfWork;
using DesignPatterns.DecoratorPattern;
using DesignPatterns.FactoryPattern.FactoryMethod;
using DesignPatterns.FactoryPattern.AbstractFactory;
using DesignPatterns.FactoryPattern.AbstractFactory.Factories;
using DesignPatterns.FacadePattern;
using DesignPatterns.ChainOfResponsibility;
using DesignPatterns.StrategyPattern.ConcreteStrategies;
using DesignPatterns.StrategyPattern;
using DesignPatterns.BuilderPattern.Builder;
using DesignPatterns.BuilderPattern.FluentBuilder;
using Autofac;
using DesignPatterns.Singleton;
using DesignPatterns.CompositePattern;

namespace DesignPatterns
{
    class Program
    {

        static private IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<StudentLiabilitiesBuilder>().As<IStudentLiabilitiesBuilder>();
            builder.RegisterType<StudentLiabilitiesFluentBuilder>().As<IStudentLiabilitiesFluentBuilder>();
            builder.RegisterType<StudentLiabilitiesDirector>();
            builder.RegisterType<StudentLiabilities>();
            builder.RegisterType<SingletonAutofac>().SingleInstance();
            return builder.Build();
        }
        static void Main(string[] args)
        {
            #region Iterator Pattern - Student Implementation, basic version
            //var students = new StudentData();
            //foreach (var item in students)
            //{
            //    Console.WriteLine(item.ToString());
            //}
            #endregion

            #region Repository Pattern
            //TODO: Implement Autofac or another IoC Framework
            //var context = new CodeFirstContext();
            //var repository = new Repository<Clans,CodeFirstContext>(context);
            //var allClans = repository.GetAll().ToList();
            #endregion

            #region Unit Of Work
            //var uow = new UnitOfWork.UnitOfWork();
            //var allClansUoW = uow.Repository<Clans>().GetAll().ToList();
            #endregion

            #region Decorator Pattern
            //var businessFlight = new BusinessFlightClass();
            //var rentalcar = new RentalCarDecorator(businessFlight);
            //rentalcar.DisplayClassOfFlight();
            //rentalcar.DisplayPricelist();
            #endregion

            #region Factory Pattern

            #region Factory Method Pattern
            //var studentFactory = new StudentFactory();
            //var dailyStudent = studentFactory.GetStudent(StudentMode.Daily);
            //dailyStudent.FirstName = "Rafał";
            //dailyStudent.Surname = "Serafin";
            //dailyStudent.DisplayFullName();
            #endregion

            #region Abstract Factory Pattern            
            //var dormitoryFactory = AbstractFactory.GetFactory(true);
            //var bedroom = dormitoryFactory.CreateBedroom();
            //var equipment = bedroom.GetEquipment();
            //foreach (var item in equipment)
            //{
            //    Console.WriteLine("{0}", item);
            //}
            #endregion


            #endregion

            #region Facade Pattern
            //var facade = new InvoiceFacade();
            //var invoice = facade.GenerateInvoice(999);
            #endregion

            #region Chain of Responsibility
            //var studentApplication = new StudentApplication("Rafał Serafin", false, 2);

            //var reservation = new ReservationStudentQualification();
            //var registration = new RegistrationStudentQualification();
            //var reserveList = new ReserveListStudentQualification();

            //reservation.SetNext(registration);
            //registration.SetNext(reserveList);

            //reservation.QualifyStudent(studentApplication);
            //Console.ReadLine();
            #endregion

            #region Strategy Pattern

            #region version without factory
            var calculator = new DormitoryCalculator_WithoutFactory();
            var result = calculator.CalculatePrice(DormitoryCountry.Canada);
            #endregion

            #region version with factory
            var calculator2 = new DormitoryCalculator_WithoutFactory();
            var result2 = calculator2.CalculatePrice(DormitoryCountry.Canada);
            #endregion

            #endregion

            #region Builder Pattern            
            var director = CompositionRoot().Resolve<StudentLiabilitiesDirector>();
            var studentLiabilities = director.Build();
            #endregion

            #region Fluent Builder Pattern
            var fluentBuilder = CompositionRoot().Resolve<IStudentLiabilitiesFluentBuilder>();
            var studentLiabilitiesByFluent = fluentBuilder
                .BuildAcademicFees()
                .BuildCourseFees()
                .BuildInsuranceFees(true)
                .GetStudentLiabilities();
            #endregion

            #region Singleton Pattern - not thread-safe
            var singletonNotThreadSafe = SingletonNotThreadSafe.Instance;
            #endregion

            #region Singleton Pattern - thread-safe
            var singletonThreadSafe = SingletonThreadSafe.Instance;
            #endregion

            #region Singleton Pattern using Autofac
            var singletonAutofac = CompositionRoot().Resolve<SingletonAutofac>();
            #endregion

            #region Composite Pattern
            var teamLeader1 = new TeamLeader("TeamLeader 1");
            var teamLeader2 = new TeamLeader("TeamLeader 2");

            var developer1 = new Developer("Developer 1");
            var developer2 = new Developer("Developer 2");
            var developer3 = new Developer("Developer 3");
            var developer4 = new Developer("Developer 4");
            var developer5 = new Developer("Developer 5");

            teamLeader1.AddSubordinate(developer1);
            teamLeader1.AddSubordinate(developer2);
            teamLeader1.AddSubordinate(developer3);
            teamLeader1.AddSubordinate(developer4);

            teamLeader2.AddSubordinate(developer5);

            var employees = new List<EmployeeComponent>() { teamLeader1, teamLeader2 };
            foreach (var employee in employees)
            {
                Console.WriteLine("{0} - number of employees {1}", employee.Name, employee.CountSubordinates());
            }
            Console.ReadLine();
            #endregion

        }
    }
}
