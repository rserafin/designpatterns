﻿using System.Collections;

namespace DesignPatterns.IteratorPattern
{
    public class StudentEnumerator : IEnumerator
    {
        private int _position;
        private ArrayList _studentList;


        public StudentEnumerator(ArrayList studentList)
        {
            _studentList = studentList;
        }

        public void Dispose() { }
        public void Reset()
        {
            _position = -1;
        }

        public object Current
        {
            get { return _studentList[_position]; }
        }

        public bool MoveNext()
        {
            return ++_position < _studentList.Count;
        }
    }
}
