﻿using System;

namespace DesignPatterns.IteratorPattern
{
    public class Student
    {
        public Guid Gid { get; private set; }
        public string Name { get; private set; }
        public string Faculty { get; private set; }

        public Student(string name, string faculty)
        {
            Gid = Guid.NewGuid();
            Name = name;
            Faculty = faculty;
        }

        public override string ToString()
        {
            return "Student: " + Name + " Faculty: " + Faculty;
        }
    }
}
