﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.IteratorPattern
{
    public class StudentData : IEnumerable
    {
        private ArrayList _students = new ArrayList();

        public StudentData()
        {
            _students.Add(new Student("Serafin", "IT"));
            _students.Add(new Student("Kowalski", "IT"));
            _students.Add(new Student("Malek", "Finance"));
            _students.Add(new Student("Modelski", "Architecture"));
        }

        public IEnumerator GetEnumerator()
        {
            return new StudentEnumerator(this._students);
        }
    }
}
