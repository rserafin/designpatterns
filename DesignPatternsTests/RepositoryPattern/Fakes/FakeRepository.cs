﻿using DesignPatterns.RepositoryPattern;
using EF_CodeFirst.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DesignPatternsTests.RepositoryPattern.Fakes
{
    public class FakeRepository<T> : IRepository<T> where T : Entity
    {
        public HashSet<T> set;
        private IQueryable<T> _queryableSet;

        public FakeRepository(IEnumerable<T> entities)
        {
            set = new HashSet<T>();
            foreach (var entity in entities)
            {
                set.Add(entity);
            }
            _queryableSet = set.AsQueryable();
        }

        public FakeRepository() : this(Enumerable.Empty<T>())
        {        
        }

        public IQueryable<T> GetAll()
        {
            return _queryableSet;
        }

        public T FindByID(int id)
        {
            return _queryableSet.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _queryableSet.Where(predicate);
        }

        public void Add(T entity)
        {
            set.Add(entity);
        }

        public void Remove(T entity)
        {
            set.Remove(entity);
        }

        public void Edit(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
