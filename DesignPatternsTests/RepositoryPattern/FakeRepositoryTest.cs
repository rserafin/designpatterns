﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using DesignPatternsTests.RepositoryPattern.Fakes;
using EF_CodeFirst.Entities;

namespace DesignPatternsTests.RepositoryPattern
{
    [TestFixture]
    public class FakeRepositoryTest
    {
        private FakeRepository<Clans> _fakeRepository;

        [SetUp]
        public void Setup()
        {
            _fakeRepository = new FakeRepository<Clans>();
        }

        [Test]
        public void ShouldAddNewEntity()
        {
            //Arrange            
            var newClan = CreateClan(1, "Fake Clan Name");

            //Act
            _fakeRepository.Add(newClan);

            //Assert
            Assert.IsTrue(_fakeRepository.set.Count == 1);
        }

        [Test]
        public void ShouldReturnAllEntities()
        {
            //Arrange
            var clansToCreate = 5;
            for (int i = 0; i < clansToCreate; i++)
            {
                _fakeRepository.Add(CreateClan(i, String.Concat("Fake Clan Name ", i)));
            }

            //Act
            var result = _fakeRepository.FindBy(x => x.Id > 2);

            //Assert
            Assert.IsTrue(result.Count() == 2);
        }

        private Clans CreateClan(int id, string clanName)
        {
            var newClan = new Clans();
            newClan.Id = id;
            newClan.ClanName = clanName;
            return newClan;
        }

    }
}
