﻿using DesignPatterns.RepositoryPattern;
using EF_CodeFirst;
using EF_CodeFirst.Entities;
using Moq;
using NUnit.Framework;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;

namespace DesignPatternsTests.RepositoryPattern
{
    [TestFixture]
    public class MockRepositoryTest
    {

        private Repository<Clans, CodeFirstContext> _repository;

        [Test]
        public void ShouldReturnAllEntities()
        {
            //Arrange
            var mockContext = new Mock<CodeFirstContext>();
            var clansMock = new Mock<DbSet<Clans>>();

            List<Clans> listOfClans = new List<Clans>
            {
                CreateClan(1, "Mock Clan 1"),
                CreateClan(2, "Mock Clan 2")
            };

            IQueryable<Clans> queryableList = listOfClans.AsQueryable();
            clansMock.As<IQueryable<Clans>>().Setup(x => x.Provider).Returns(queryableList.Provider);
            clansMock.As<IQueryable<Clans>>().Setup(x => x.Expression).Returns(queryableList.Expression);
            clansMock.As<IQueryable<Clans>>().Setup(x => x.ElementType).Returns(queryableList.ElementType);
            clansMock.As<IQueryable<Clans>>().Setup(x => x.GetEnumerator()).Returns(queryableList.GetEnumerator());

            mockContext.Setup(x => x.Set<Clans>()).Returns(clansMock.Object);
            _repository = new Repository<Clans, CodeFirstContext>(mockContext.Object);

            //Act
            var result = _repository.GetAll();

            //Assert
            Assert.AreEqual(2, result.Count());
        }


        private Clans CreateClan(int id, string clanName)
        {
            var newClan = new Clans();
            newClan.Id = id;
            newClan.ClanName = clanName;
            return newClan;
        }

    }
}
